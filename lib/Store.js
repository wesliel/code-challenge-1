/**
 * @module lib
 * Store.js
 * @author Weslie Leung
 * @description A simple JSON store object using file system
 */

'use strict'

const fs = require('fs')
const DEFAULT_FILENAME = 'data.json'

/**
 * Using JSON API's serialize/deserialize methods, copies and returns the object.
 * This ensures the object is immutable
 * @param {Object} jsonData JSON data to be copied
 * @returns {Object} Copy of data
 */
const copyJson = (jsonData) => JSON.parse(JSON.stringify(jsonData))

/**
 * Checks a param is a certain type
 * @param {*} param
 * @param {string} typeName
 */
const checkParamType = (param, typeName) => {
  if (typeof param !== typeName) {
    throw new Error(`param must be a ${typeName}`)
  }
}

const getValueType = (value) => {
  const arrayRegex = /^\[(\W|\w)*\]$/
  const objectRegex = /^\{(\W|\w)*\}$/

  if (arrayRegex.test(value)) return 'array'
  if (objectRegex.test(value)) return 'object'
  if (!isNaN(Number(value))) return 'number'

  return 'string'
}

/**
 * @class
 *
 * A Store uses file system to maintain a copy of JSON data.
 * It has basic functionalities such as list, get, add, remove, and clear
 */
class Store {

  constructor(fileName = DEFAULT_FILENAME) {
    this._fileName = fileName
    this._data = this._load()
  }

  /**
   * Loads JSON data from initialized file name
   * @throws If reading operation fails
   */
  _load() {
    try {
      return JSON.parse(fs.readFileSync(this._fileName, 'utf8'))
    } catch (error) {
      throw new Error(`Unable to read file: ${error.message}`)
    }
  }

  /**
   * Saves current data in memory to file
   * @throws If writing operation fails
   */
  _save() {
    try {
      fs.writeFileSync(this._fileName, JSON.stringify(this._data))
    } catch (error) {
      throw new Error(`Unable to write file: ${error.message}`)
    }
  }

  /**
   * Returns a copy current data store.
   * This prevents any changes outside of the class mutating this data directly.
   * @returns {Object}
   */
  list() {
    return copyJson(this._data)
  }

  /**
   * Given a key, returns its value from current data store
   * @param {string} key
   * @returns {*}
   * @throws If key does not exist in store
   */
  get(key) {
    checkParamType(key, 'string')
    return this._data.hasOwnProperty(key) ? copyJson(this._data)[key] : undefined
  }

  /**
   * Given a key and value, updates and saves data.
   * If key already exists in store, it'd simply overwrite it.
   * @param {string} key
   * @param {*} value
   * @returns {Object}
   */
  add(key, value) {
    let newPair
    checkParamType(key, 'string')

    if (getValueType(value) === 'string') {
      newPair = JSON.parse(`{"${key}": "${value}"}`)
    } else {
      newPair = JSON.parse(`{"${key}": ${value}}`)
    }

    this._data = Object.assign({}, this._data, newPair)
    this._save()
    return copyJson(this._data)
  }

  /**
   * Given a key, removes its value then saves data
   * @param {string} key
   */
  remove(key) {
    checkParamType(key, 'string')
    if (this._data.hasOwnProperty(key)) {
      delete this._data[key]
      this._save()
    }
    return copyJson(this._data)
  }

  /**
   * Clears the entire store then saves
   * @returns {Object}
   */
  clear() {
    this._data = {}
    this._save()
    return {}
  }
}

module.exports = Store