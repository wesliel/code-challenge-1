#!/usr/bin/env node

const Store = require('./lib/Store')

// deconstruct arguments to extract command, key, and value
const [n, e, command, key, value] = process.argv
const DATA_FILE = 'data.json'
const store = new Store(DATA_FILE)

const write = (message, logLevel = 'out') => {
  if (logLevel === 'error') {
    process.stderr.write(`${message}\n`)
  } else {
    process.stdout.write(`${message}\n`)
  }
}

try{
  switch(command) {
  case 'list':
    write(`${JSON.stringify(
      store.list()
    )}`)
    break;
  case 'get':
    write(`${JSON.stringify(
      store.get(key)
    )}`)
    break;
  case 'add':
    write(`${JSON.stringify(
      store.add(key, value)
    )}`)
    break;
  case 'remove':
    write(`${JSON.stringify(
      store.remove(key)
    )}`)
    break;
  case 'clear':
    write(`${JSON.stringify(
      store.clear()
    )}`)
    break;
  default:
    write(`Unknown command: ${command}`, 'error')
  }
} catch (err) {
  write('Invalid format: JSON objects should be in {\\"a\\": 123} format')
}