/**
 * @module test
 * Store.js
 * @author Weslie Leung
 * @description Unit test file for {Store}
 */

const { expect, assert } = require('chai')
const sinon = require('sinon')
const Store = require('../lib/Store')

describe('Store', function() {

  describe('open and read file', function() {

    it('should throw file not found exception when invalid fileName is given', function() {
      try {
        const wrongStore = new Store('wrongStore.json')
        assert.fail()
      } catch (error) {
        expect(error.message).to.contain('Unable to read')
      }
    })

  })

  describe('features', function() {
    /**
     * Test store should contain this test data
     * {
     *    "colour": "green",
     *   "fruit": "banana"
     * }
     */

    let store

    before(function() {
      // stub save to file so test data file remains unchanged
      sinon.stub(Store.prototype, '_save').callsFake(() => (true))
      sinon.stub(Store.prototype, '_load').callsFake(() => ({
        colour: 'green',
        fruit: 'banana'
      }))

      store = new Store()
    })

    it('should list data', function() {
      const data = store.list()
      expect(Object.keys(data).length).to.eq(2)
    })

    it('should get value given a key', function() {
      const fruits = store.get('fruit')
      expect(fruits).to.contain('banana')
    })

    it('should add key/value', function() {
      const data = store.add('nut', 'hazelnut')
      expect(data.nut).to.eq('hazelnut')
    })

    it('should add key/value with array', function() {
      const data = store.add('sports', '["hockey", "cycling", "snowboarding"]')
      store.list()
      expect(data.sports[0]).to.eq('hockey')
    })

    it('should add key/value with number', function() {
      const data = store.add('number', '1')
      expect(data.number).to.eq(1)
    })

    it('should add key/value with object', function() {
      const data = store.add('object', '{"a": 1}')
      expect(data.object.a).to.eq(1)
    })

    it('should replace key/value if key already exists', function() {
      const data = store.add('fruit', 'apple')
      expect(data.fruit).to.eq('apple')
    })

    it('should remove data', function() {
      const data = store.remove('fruit')
      expect(Object.keys(data)).to.not.contain('fruit')
    })

    it('should not affect data when removing a key that does not exist', function() {
      const oldData = store.list()
      store.remove('meat')
      const newData = store.list()

      expect(JSON.stringify(oldData)).to.eq(JSON.stringify(newData))
    })

    it('should return undefined when getting data of a key that does not exist', function() {
      const meats = store.get('meat')
      expect(typeof meats).to.eq('undefined')
    })

    it('should throw an exception when invalid key is given', function() {
      try {
        store.get({})
      } catch (error) {
        expect(error.message).to.contain('string')
      }
    })

    after(function() {
      // clean up stub
      store._save.restore()
    })
  })

})